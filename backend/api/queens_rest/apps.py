from django.apps import AppConfig


class QueensRestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "queens_rest"
